# Data Logger
*Generated from [ChatGPT](https://chat.openai.com/chat), compiled by human*

![took_er_jobs](resources/tookjobs.jpg)

Conversation with [ChatGPT](https://chat.openai.com/chat) that led to this implementation is recorded, and it can be found on following links:
* [First conversation](https://test-driven_development.gitlab.io/chatgpt/conversation_1/Check%20out%20this%20ShareGPT%20conversation.html)
* [Second conversation](https://test-driven_development.gitlab.io/chatgpt/conversation_2/Check%20out%20this%20ShareGPT%20conversation.html)
* [Third conversation](https://test-driven_development.gitlab.io/chatgpt/conversation_3/Check%20out%20this%20ShareGPT%20conversation.html)
* [Fourth conversation](https://test-driven_development.gitlab.io/chatgpt/conversation_4/Check%20out%20this%20ShareGPT%20conversation.html)
* [Fifth conversation](https://test-driven_development.gitlab.io/chatgpt/conversation_5/Check%20out%20this%20ShareGPT%20conversation.html)

Data logger's Documentation:
* [Executable specifications](data_logger/data_logger.feature)
* [Software Architecture Document](data_logger/data_logger_sad.md)
* [CI/CD](https://gitlab.com/test-driven_development/chatgpt/-/pipelines)


---

## The Data Logger Blues

Well, the data logger's a mighty fine tool<br>
For keeping track of all your data, fool<br>
It writes and it logs, it never forgets<br>
All your messages and data, no matter the threat<br>

It's a faithful companion, through thick and thin<br>
It'll keep all your data safe, tucked within<br>
A file on your computer, or maybe a cloud<br>
Wherever you choose to store it, it's proud<br>

But watch out for errors, and files that ain't writable<br>
The data logger'll let you know, it's not inexplicable<br>
And if the file don't exist, don't you worry<br>
The data logger'll create it, in a hurry<br>

So if you've got data, and you don't know what to do<br>
Just give the data logger a try, it'll see you through<br>
It's a faithful friend, through good times and bad<br>
It'll keep all your data safe, it's not so sad<br>

So don't be afraid, to give it a try<br>
The data logger's here to help, don't ask me why<br>
It's a tool like no other, it's one of a kind<br>
So give it a chance, and you'll see what you'll find.<br>

*ChatGPT - Bob Dylan style*