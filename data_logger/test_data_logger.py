import unittest
import os
import shutil
import stat
from data_logger import DataLogger


class TestDataLogger(unittest.TestCase):
    def tearDown(self):
        filename = "log.txt"
        if os.path.exists(filename):
            os.remove(filename)

        dirname = "nonexistent"
        if os.path.exists(dirname):
            shutil.rmtree(dirname)

    def test_log_message(self):
        # Given a data logger that is initialized with a valid file path
        file_path = "log.txt"
        logger = DataLogger(file_path)

        # When the data logger is used to log a message
        message = "This is a test message"
        logger.log(message)

        # Then the message should be written to the file
        with open(file_path, "r", encoding="utf8") as f:
            logged_message = f.read()
        self.assertEqual(logged_message, message)

    def test_log_message_with_nonexistent_file(self):
        # Given a data logger that is initialized with a file path that does not exist
        file_path = "nonexistent/log.txt"
        logger = DataLogger(file_path)

        # When the data logger is used to log a message
        message = "This is a test message"
        logger.log(message)

        # Then the file should be created and the message should be written to it
        with open(file_path, "r", encoding="utf8") as f:
            logged_message = f.read()
        self.assertEqual(logged_message, message)

    def test_log_message_to_existing_file(self):
        # Given a data logger that is initialized with a file path that exists and has data in it
        file_path = "log.txt"
        logger = DataLogger(file_path)
        with open(file_path, "w", encoding="utf8") as f:
            f.write("Initial data\n")

        # When the data logger is used to log a message
        message = "This is a test message"
        logger.log(message)

        # Then the message should be appended to the end of the file
        with open(file_path, "r", encoding="utf8") as f:
            logged_message = f.read()
        self.assertEqual(logged_message, "Initial data\nThis is a test message")

    def test_log_message_with_timestamp(self):
        # Given a data logger that is initialized with a file path and a timestamp format
        file_path = "log.txt"
        timestamp_format = "%Y-%m-%d %H:%M:%S"
        logger = DataLogger(file_path, timestamp_format)

        # When the data logger is used to log a message
        message = "This is a test message"
        logger.log(message)

        # Then the message should be written to the file with the specified timestamp format
        with open(file_path, "r") as f:
            logged_message = f.read()
        import datetime

        self.assertTrue(
            logged_message.startswith(
                datetime.datetime.now().strftime(timestamp_format)
            )
        )
        self.assertTrue(logged_message.endswith(message))

    def test_log_message_with_custom_format(self):
        # Given a data logger that is initialized with a file path and a custom log format
        file_path = "log.txt"
        timestamp_format = "%Y-%m-%d %H:%M:%S"
        log_format = "[{level}] {timestamp} - {message}"
        logger = DataLogger(
            file_path, timestamp_format=timestamp_format, log_format=log_format
        )

        # When the data logger is used to log a message
        message = "This is a test message"
        logger.log(message, level="INFO")

        # Then the message should be written to the file in the specified log format
        with open(file_path, "r") as f:
            logged_message = f.read()
        self.assertEqual(logged_message, logger.logged_message)


if __name__ == "__main__":
    unittest.main()
