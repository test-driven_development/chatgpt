import os
import datetime


class DataLogger:
    def __init__(self, file_path, timestamp_format=None, log_format=None):
        """Initialize a DataLogger instance.

        Args:
            file_path: The file path for the log file.
            timestamp_format: The format for the timestamp, as a string.
            log_format: The format for the logged message, as a string.
        """
        self.file_path = file_path
        self.timestamp_format = timestamp_format
        self.log_format = log_format
        self.logged_message = ""
        self.init_file()

    def init_file(self):
        """Create the directory for the log file, if it does not exist, and
        create the log file if it does not exist.
        """
        dir_name = os.path.dirname(self.file_path)
        if not os.path.exists(dir_name) and dir_name != "":
            os.makedirs(dir_name)

        if not os.path.exists(self.file_path):
            with open(self.file_path, "w", encoding="utf8") as f:
                f.write("")

    def log(self, message, level=None):
        """Log a message to the log file.

        Args:
            message: The message to log, as a string.
            level: The level of the message, as a string.
        """
        # Format the message with the timestamp and log format
        timestamp = (
            datetime.datetime.now().strftime(self.timestamp_format)
            if self.timestamp_format
            else ""
        )

        if self.log_format:
            self.logged_message = self.log_format.format(
                level=level, timestamp=timestamp, message=message
            )
        elif timestamp:
            self.logged_message = f"{timestamp} {message}"
        else:
            self.logged_message = message

        # Append the logged message to the log file
        try:
            with open(self.file_path, "a", encoding="utf8") as f:
                f.write(self.logged_message)
        except IOError as e:
            raise e
