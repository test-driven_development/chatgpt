Feature: Data logger functionality

Scenario: Data logger writes message to file
    Given a data logger that is initialized with a valid file path
    When the data logger is used to log a message
    Then the message should be written to the file

Scenario: Data logger throws error when file is not writable
    Given a data logger that is initialized with a file path that is not writable
    When the data logger is used to log a message
    Then an error should be thrown indicating that the file is not writable

Scenario: Data logger creates file and writes message
    Given a data logger that is initialized with a file path that does not exist
    When the data logger is used to log a message
    Then the file should be created and the message should be written to it

Scenario: Data logger appends message to end of file
    Given a data logger that is initialized with a file path that exists and has data in it
    When the data logger is used to log a message
    Then the message should be appended to the end of the file

Scenario: Data logger writes message to file with timestamp format
    Given a data logger that is initialized with a file path and a timestamp format
    When the data logger is used to log a message
    Then the message should be written to the file with the specified timestamp format

Scenario: Data logger writes message to file in custom log format
    Given a data logger that is initialized with a file path and a custom log format
    When the data logger is used to log a message
    Then the message should be written to the file in the specified log format
