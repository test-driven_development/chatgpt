(self.webpackChunk_N_E=self.webpackChunk_N_E||[]).push([[747],{8814:function(e,t,r){"use strict";r.d(t,{Z:function(){return o}});var a=r(4515);let o=(0,a.Z)("ChevronLeft",[["polyline",{points:"15 18 9 12 15 6",key:"kvxz59"}]])},3164:function(e,t,r){(window.__NEXT_P=window.__NEXT_P||[]).push(["/c/[id]",function(){return r(2632)}])},2632:function(e,t,r){"use strict";let a,o,n;r.r(t),r.d(t,{__N_SSG:function(){return ti},default:function(){return tl}});var s,i=r(5893),l=r(5675),c=r.n(l),d=r(4184),u=r.n(d);function m(){return(0,i.jsx)("div",{className:"bg-[#10A37F] border-0 flex items-center justify-center h-[30px] w-[30px] rounded-sm p-[0.25rem]",children:(0,i.jsx)("svg",{width:"41",height:"41",viewBox:"0 0 41 41",fill:"rgb(16, 163, 127)",xmlns:"http://www.w3.org/2000/svg",strokeWidth:"1.5",className:"h-6 w-6",children:(0,i.jsx)("path",{d:"M37.5324 16.8707C37.9808 15.5241 38.1363 14.0974 37.9886 12.6859C37.8409 11.2744 37.3934 9.91076 36.676 8.68622C35.6126 6.83404 33.9882 5.3676 32.0373 4.4985C30.0864 3.62941 27.9098 3.40259 25.8215 3.85078C24.8796 2.7893 23.7219 1.94125 22.4257 1.36341C21.1295 0.785575 19.7249 0.491269 18.3058 0.500197C16.1708 0.495044 14.0893 1.16803 12.3614 2.42214C10.6335 3.67624 9.34853 5.44666 8.6917 7.47815C7.30085 7.76286 5.98686 8.3414 4.8377 9.17505C3.68854 10.0087 2.73073 11.0782 2.02839 12.312C0.956464 14.1591 0.498905 16.2988 0.721698 18.4228C0.944492 20.5467 1.83612 22.5449 3.268 24.1293C2.81966 25.4759 2.66413 26.9026 2.81182 28.3141C2.95951 29.7256 3.40701 31.0892 4.12437 32.3138C5.18791 34.1659 6.8123 35.6322 8.76321 36.5013C10.7141 37.3704 12.8907 37.5973 14.9789 37.1492C15.9208 38.2107 17.0786 39.0587 18.3747 39.6366C19.6709 40.2144 21.0755 40.5087 22.4946 40.4998C24.6307 40.5054 26.7133 39.8321 28.4418 38.5772C30.1704 37.3223 31.4556 35.5506 32.1119 33.5179C33.5027 33.2332 34.8167 32.6547 35.9659 31.821C37.115 30.9874 38.0728 29.9178 38.7752 28.684C39.8458 26.8371 40.3023 24.6979 40.0789 22.5748C39.8556 20.4517 38.9639 18.4544 37.5324 16.8707ZM22.4978 37.8849C20.7443 37.8874 19.0459 37.2733 17.6994 36.1501C17.7601 36.117 17.8666 36.0586 17.936 36.0161L25.9004 31.4156C26.1003 31.3019 26.2663 31.137 26.3813 30.9378C26.4964 30.7386 26.5563 30.5124 26.5549 30.2825V19.0542L29.9213 20.998C29.9389 21.0068 29.9541 21.0198 29.9656 21.0359C29.977 21.052 29.9842 21.0707 29.9867 21.0902V30.3889C29.9842 32.375 29.1946 34.2791 27.7909 35.6841C26.3872 37.0892 24.4838 37.8806 22.4978 37.8849ZM6.39227 31.0064C5.51397 29.4888 5.19742 27.7107 5.49804 25.9832C5.55718 26.0187 5.66048 26.0818 5.73461 26.1244L13.699 30.7248C13.8975 30.8408 14.1233 30.902 14.3532 30.902C14.583 30.902 14.8088 30.8408 15.0073 30.7248L24.731 25.1103V28.9979C24.7321 29.0177 24.7283 29.0376 24.7199 29.0556C24.7115 29.0736 24.6988 29.0893 24.6829 29.1012L16.6317 33.7497C14.9096 34.7416 12.8643 35.0097 10.9447 34.4954C9.02506 33.9811 7.38785 32.7263 6.39227 31.0064ZM4.29707 13.6194C5.17156 12.0998 6.55279 10.9364 8.19885 10.3327C8.19885 10.4013 8.19491 10.5228 8.19491 10.6071V19.808C8.19351 20.0378 8.25334 20.2638 8.36823 20.4629C8.48312 20.6619 8.64893 20.8267 8.84863 20.9404L18.5723 26.5542L15.206 28.4979C15.1894 28.5089 15.1703 28.5155 15.1505 28.5173C15.1307 28.5191 15.1107 28.516 15.0924 28.5082L7.04046 23.8557C5.32135 22.8601 4.06716 21.2235 3.55289 19.3046C3.03862 17.3858 3.30624 15.3413 4.29707 13.6194ZM31.955 20.0556L22.2312 14.4411L25.5976 12.4981C25.6142 12.4872 25.6333 12.4805 25.6531 12.4787C25.6729 12.4769 25.6928 12.4801 25.7111 12.4879L33.7631 17.1364C34.9967 17.849 36.0017 18.8982 36.6606 20.1613C37.3194 21.4244 37.6047 22.849 37.4832 24.2684C37.3617 25.6878 36.8382 27.0432 35.9743 28.1759C35.1103 29.3086 33.9415 30.1717 32.6047 30.6641C32.6047 30.5947 32.6047 30.4733 32.6047 30.3889V21.188C32.6066 20.9586 32.5474 20.7328 32.4332 20.5338C32.319 20.3348 32.154 20.1698 31.955 20.0556ZM35.3055 15.0128C35.2464 14.9765 35.1431 14.9142 35.069 14.8717L27.1045 10.2712C26.906 10.1554 26.6803 10.0943 26.4504 10.0943C26.2206 10.0943 25.9948 10.1554 25.7963 10.2712L16.0726 15.8858V11.9982C16.0715 11.9783 16.0753 11.9585 16.0837 11.9405C16.0921 11.9225 16.1048 11.9068 16.1207 11.8949L24.1719 7.25025C25.4053 6.53903 26.8158 6.19376 28.2383 6.25482C29.6608 6.31589 31.0364 6.78077 32.2044 7.59508C33.3723 8.40939 34.2842 9.53945 34.8334 10.8531C35.3826 12.1667 35.5464 13.6095 35.3055 15.0128ZM14.2424 21.9419L10.8752 19.9981C10.8576 19.9893 10.8423 19.9763 10.8309 19.9602C10.8195 19.9441 10.8122 19.9254 10.8098 19.9058V10.6071C10.8107 9.18295 11.2173 7.78848 11.9819 6.58696C12.7466 5.38544 13.8377 4.42659 15.1275 3.82264C16.4173 3.21869 17.8524 2.99464 19.2649 3.1767C20.6775 3.35876 22.0089 3.93941 23.1034 4.85067C23.0427 4.88379 22.937 4.94215 22.8668 4.98473L14.9024 9.58517C14.7025 9.69878 14.5366 9.86356 14.4215 10.0626C14.3065 10.2616 14.2466 10.4877 14.2479 10.7175L14.2424 21.9419ZM16.071 17.9991L20.4018 15.4978L24.7325 17.9975V22.9985L20.4018 25.4983L16.071 22.9985V17.9991Z",fill:"#fff"})})})}var p=r(2734),f=r.n(p),h=r(4515);let x=(0,h.Z)("Check",[["polyline",{points:"20 6 9 17 4 12",key:"10jjfj"}]]),g=(0,h.Z)("Link",[["path",{d:"M10 13a5 5 0 0 0 7.54.54l3-3a5 5 0 0 0-7.07-7.07l-1.72 1.71",key:"1cjeqo"}],["path",{d:"M14 11a5 5 0 0 0-7.54-.54l-3 3a5 5 0 0 0 7.07 7.07l1.71-1.71",key:"19qd67"}]]);var y=r(4464),v=r(1664),b=r.n(v),w=r(1163),C=r(7294);let j={data:""},N=e=>"object"==typeof window?((e?e.querySelector("#_goober"):window._goober)||Object.assign((e||document.head).appendChild(document.createElement("style")),{innerHTML:" ",id:"_goober"})).firstChild:e||j,E=/(?:([\u0080-\uFFFF\w-%@]+) *:? *([^{;]+?);|([^;}{]*?) *{)|(}\s*)/g,k=/\/\*[^]*?\*\/|  +/g,T=/\n+/g,P=(e,t)=>{let r="",a="",o="";for(let n in e){let s=e[n];"@"==n[0]?"i"==n[1]?r=n+" "+s+";":a+="f"==n[1]?P(s,n):n+"{"+P(s,"k"==n[1]?"":t)+"}":"object"==typeof s?a+=P(s,t?t.replace(/([^,])+/g,e=>n.replace(/(^:.*)|([^,])+/g,t=>/&/.test(t)?t.replace(/&/g,e):e?e+" "+t:t)):n):null!=s&&(n=/^--/.test(n)?n:n.replace(/[A-Z]/g,"-$&").toLowerCase(),o+=P.p?P.p(n,s):n+":"+s+";")}return r+(t&&o?t+"{"+o+"}":o)+a},S={},M=e=>{if("object"==typeof e){let t="";for(let r in e)t+=r+M(e[r]);return t}return e},_=(e,t,r,a,o)=>{var n,s;let i=M(e),l=S[i]||(S[i]=(e=>{let t=0,r=11;for(;t<e.length;)r=101*r+e.charCodeAt(t++)>>>0;return"go"+r})(i));if(!S[l]){let c=i!==e?e:(e=>{let t,r,a=[{}];for(;t=E.exec(e.replace(k,""));)t[4]?a.shift():t[3]?(r=t[3].replace(T," ").trim(),a.unshift(a[0][r]=a[0][r]||{})):a[0][t[1]]=t[2].replace(T," ").trim();return a[0]})(e);S[l]=P(o?{["@keyframes "+l]:c}:c,r?"":"."+l)}let d=r&&S.g?S.g:null;return r&&(S.g=S[l]),n=S[l],s=t,d?s.data=s.data.replace(d,n):-1===s.data.indexOf(n)&&(s.data=a?n+s.data:s.data+n),l},O=(e,t,r)=>e.reduce((e,a,o)=>{let n=t[o];if(n&&n.call){let s=n(r),i=s&&s.props&&s.props.className||/^go/.test(s)&&s;n=i?"."+i:s&&"object"==typeof s?s.props?"":P(s,""):!1===s?"":s}return e+a+(null==n?"":n)},"");function L(e){let t=this||{},r=e.call?e(t.p):e;return _(r.unshift?r.raw?O(r,[].slice.call(arguments,1),t.p):r.reduce((e,r)=>Object.assign(e,r&&r.call?r(t.p):r),{}):r,N(t.target),t.g,t.o,t.k)}L.bind({g:1});let D,R,Z,I=L.bind({k:1});function z(e,t){let r=this||{};return function(){let a=arguments;function o(n,s){let i=Object.assign({},n),l=i.className||o.className;r.p=Object.assign({theme:R&&R()},i),r.o=/ *go\d+/.test(l),i.className=L.apply(r,a)+(l?" "+l:""),t&&(i.ref=s);let c=e;return e[0]&&(c=i.as||e,delete i.as),Z&&c[0]&&Z(i),D(c,i)}return t?t(o):o}}var F=e=>"function"==typeof e,q=(e,t)=>F(e)?e(t):e,$=(o=0,()=>(++o).toString()),H=()=>{if(void 0===n&&"u">typeof window){let e=matchMedia("(prefers-reduced-motion: reduce)");n=!e||e.matches}return n},V=new Map,A=e=>{if(V.has(e))return;let t=setTimeout(()=>{V.delete(e),U({type:4,toastId:e})},1e3);V.set(e,t)},G=e=>{let t=V.get(e);t&&clearTimeout(t)},B=(e,t)=>{switch(t.type){case 0:return{...e,toasts:[t.toast,...e.toasts].slice(0,20)};case 1:return t.toast.id&&G(t.toast.id),{...e,toasts:e.toasts.map(e=>e.id===t.toast.id?{...e,...t.toast}:e)};case 2:let{toast:r}=t;return e.toasts.find(e=>e.id===r.id)?B(e,{type:1,toast:r}):B(e,{type:0,toast:r});case 3:let{toastId:a}=t;return a?A(a):e.toasts.forEach(e=>{A(e.id)}),{...e,toasts:e.toasts.map(e=>e.id===a||void 0===a?{...e,visible:!1}:e)};case 4:return void 0===t.toastId?{...e,toasts:[]}:{...e,toasts:e.toasts.filter(e=>e.id!==t.toastId)};case 5:return{...e,pausedAt:t.time};case 6:let o=t.time-(e.pausedAt||0);return{...e,pausedAt:void 0,toasts:e.toasts.map(e=>({...e,pauseDuration:e.pauseDuration+o}))}}},W=[],X={toasts:[],pausedAt:void 0},U=e=>{X=B(X,e),W.forEach(e=>{e(X)})},K={blank:4e3,error:4e3,success:2e3,loading:1/0,custom:4e3},J=(e={})=>{let[t,r]=(0,C.useState)(X);(0,C.useEffect)(()=>(W.push(r),()=>{let e=W.indexOf(r);e>-1&&W.splice(e,1)}),[t]);let a=t.toasts.map(t=>{var r,a;return{...e,...e[t.type],...t,duration:t.duration||(null==(r=e[t.type])?void 0:r.duration)||(null==e?void 0:e.duration)||K[t.type],style:{...e.style,...null==(a=e[t.type])?void 0:a.style,...t.style}}});return{...t,toasts:a}},Y=(e,t="blank",r)=>({createdAt:Date.now(),visible:!0,type:t,ariaProps:{role:"status","aria-live":"polite"},message:e,pauseDuration:0,...r,id:(null==r?void 0:r.id)||$()}),Q=e=>(t,r)=>{let a=Y(t,e,r);return U({type:2,toast:a}),a.id},ee=(e,t)=>Q("blank")(e,t);ee.error=Q("error"),ee.success=Q("success"),ee.loading=Q("loading"),ee.custom=Q("custom"),ee.dismiss=e=>{U({type:3,toastId:e})},ee.remove=e=>U({type:4,toastId:e}),ee.promise=(e,t,r)=>{let a=ee.loading(t.loading,{...r,...null==r?void 0:r.loading});return e.then(e=>(ee.success(q(t.success,e),{id:a,...r,...null==r?void 0:r.success}),e)).catch(e=>{ee.error(q(t.error,e),{id:a,...r,...null==r?void 0:r.error})}),e};var et=(e,t)=>{U({type:1,toast:{id:e,height:t}})},er=()=>{U({type:5,time:Date.now()})},ea=e=>{let{toasts:t,pausedAt:r}=J(e);(0,C.useEffect)(()=>{if(r)return;let e=Date.now(),a=t.map(t=>{if(t.duration===1/0)return;let r=(t.duration||0)+t.pauseDuration-(e-t.createdAt);if(r<0){t.visible&&ee.dismiss(t.id);return}return setTimeout(()=>ee.dismiss(t.id),r)});return()=>{a.forEach(e=>e&&clearTimeout(e))}},[t,r]);let a=(0,C.useCallback)(()=>{r&&U({type:6,time:Date.now()})},[r]),o=(0,C.useCallback)((e,r)=>{let{reverseOrder:a=!1,gutter:o=8,defaultPosition:n}=r||{},s=t.filter(t=>(t.position||n)===(e.position||n)&&t.height),i=s.findIndex(t=>t.id===e.id),l=s.filter((e,t)=>t<i&&e.visible).length;return s.filter(e=>e.visible).slice(...a?[l+1]:[0,l]).reduce((e,t)=>e+(t.height||0)+o,0)},[t]);return{toasts:t,handlers:{updateHeight:et,startPause:er,endPause:a,calculateOffset:o}}},eo=z("div")`
  width: 20px;
  opacity: 0;
  height: 20px;
  border-radius: 10px;
  background: ${e=>e.primary||"#ff4b4b"};
  position: relative;
  transform: rotate(45deg);

  animation: ${I`
from {
  transform: scale(0) rotate(45deg);
	opacity: 0;
}
to {
 transform: scale(1) rotate(45deg);
  opacity: 1;
}`} 0.3s cubic-bezier(0.175, 0.885, 0.32, 1.275)
    forwards;
  animation-delay: 100ms;

  &:after,
  &:before {
    content: '';
    animation: ${I`
from {
  transform: scale(0);
  opacity: 0;
}
to {
  transform: scale(1);
  opacity: 1;
}`} 0.15s ease-out forwards;
    animation-delay: 150ms;
    position: absolute;
    border-radius: 3px;
    opacity: 0;
    background: ${e=>e.secondary||"#fff"};
    bottom: 9px;
    left: 4px;
    height: 2px;
    width: 12px;
  }

  &:before {
    animation: ${I`
from {
  transform: scale(0) rotate(90deg);
	opacity: 0;
}
to {
  transform: scale(1) rotate(90deg);
	opacity: 1;
}`} 0.15s ease-out forwards;
    animation-delay: 180ms;
    transform: rotate(90deg);
  }
`,en=z("div")`
  width: 12px;
  height: 12px;
  box-sizing: border-box;
  border: 2px solid;
  border-radius: 100%;
  border-color: ${e=>e.secondary||"#e0e0e0"};
  border-right-color: ${e=>e.primary||"#616161"};
  animation: ${I`
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(360deg);
  }
`} 1s linear infinite;
`,es=z("div")`
  width: 20px;
  opacity: 0;
  height: 20px;
  border-radius: 10px;
  background: ${e=>e.primary||"#61d345"};
  position: relative;
  transform: rotate(45deg);

  animation: ${I`
from {
  transform: scale(0) rotate(45deg);
	opacity: 0;
}
to {
  transform: scale(1) rotate(45deg);
	opacity: 1;
}`} 0.3s cubic-bezier(0.175, 0.885, 0.32, 1.275)
    forwards;
  animation-delay: 100ms;
  &:after {
    content: '';
    box-sizing: border-box;
    animation: ${I`
0% {
	height: 0;
	width: 0;
	opacity: 0;
}
40% {
  height: 0;
	width: 6px;
	opacity: 1;
}
100% {
  opacity: 1;
  height: 10px;
}`} 0.2s ease-out forwards;
    opacity: 0;
    animation-delay: 200ms;
    position: absolute;
    border-right: 2px solid;
    border-bottom: 2px solid;
    border-color: ${e=>e.secondary||"#fff"};
    bottom: 6px;
    left: 6px;
    height: 10px;
    width: 6px;
  }
`,ei=z("div")`
  position: absolute;
`,el=z("div")`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  min-width: 20px;
  min-height: 20px;
`,ec=z("div")`
  position: relative;
  transform: scale(0.6);
  opacity: 0.4;
  min-width: 20px;
  animation: ${I`
from {
  transform: scale(0.6);
  opacity: 0.4;
}
to {
  transform: scale(1);
  opacity: 1;
}`} 0.3s 0.12s cubic-bezier(0.175, 0.885, 0.32, 1.275)
    forwards;
`,ed=({toast:e})=>{let{icon:t,type:r,iconTheme:a}=e;return void 0!==t?"string"==typeof t?C.createElement(ec,null,t):t:"blank"===r?null:C.createElement(el,null,C.createElement(en,{...a}),"loading"!==r&&C.createElement(ei,null,"error"===r?C.createElement(eo,{...a}):C.createElement(es,{...a})))},eu=e=>`
0% {transform: translate3d(0,${-200*e}%,0) scale(.6); opacity:.5;}
100% {transform: translate3d(0,0,0) scale(1); opacity:1;}
`,em=e=>`
0% {transform: translate3d(0,0,-1px) scale(1); opacity:1;}
100% {transform: translate3d(0,${-150*e}%,-1px) scale(.6); opacity:0;}
`,ep=z("div")`
  display: flex;
  align-items: center;
  background: #fff;
  color: #363636;
  line-height: 1.3;
  will-change: transform;
  box-shadow: 0 3px 10px rgba(0, 0, 0, 0.1), 0 3px 3px rgba(0, 0, 0, 0.05);
  max-width: 350px;
  pointer-events: auto;
  padding: 8px 10px;
  border-radius: 8px;
`,ef=z("div")`
  display: flex;
  justify-content: center;
  margin: 4px 10px;
  color: inherit;
  flex: 1 1 auto;
  white-space: pre-line;
`,eh=(e,t)=>{let r=e.includes("top")?1:-1,[a,o]=H()?["0%{opacity:0;} 100%{opacity:1;}","0%{opacity:1;} 100%{opacity:0;}"]:[eu(r),em(r)];return{animation:t?`${I(a)} 0.35s cubic-bezier(.21,1.02,.73,1) forwards`:`${I(o)} 0.4s forwards cubic-bezier(.06,.71,.55,1)`}},ex=C.memo(({toast:e,position:t,style:r,children:a})=>{let o=e.height?eh(e.position||t||"top-center",e.visible):{opacity:0},n=C.createElement(ed,{toast:e}),s=C.createElement(ef,{...e.ariaProps},q(e.message,e));return C.createElement(ep,{className:e.className,style:{...o,...r,...e.style}},"function"==typeof a?a({icon:n,message:s}):C.createElement(C.Fragment,null,n,s))});s=C.createElement,P.p=void 0,D=s,R=void 0,Z=void 0;var eg=({id:e,className:t,style:r,onHeightUpdate:a,children:o})=>{let n=C.useCallback(t=>{if(t){let r=()=>{a(e,t.getBoundingClientRect().height)};r(),new MutationObserver(r).observe(t,{subtree:!0,childList:!0,characterData:!0})}},[e,a]);return C.createElement("div",{ref:n,className:t,style:r},o)},ey=(e,t)=>{let r=e.includes("top"),a=e.includes("center")?{justifyContent:"center"}:e.includes("right")?{justifyContent:"flex-end"}:{};return{left:0,right:0,display:"flex",position:"absolute",transition:H()?void 0:"all 230ms cubic-bezier(.21,1.02,.73,1)",transform:`translateY(${t*(r?1:-1)}px)`,...r?{top:0}:{bottom:0},...a}},ev=L`
  z-index: 9999;
  > * {
    pointer-events: auto;
  }
`,eb=({reverseOrder:e,position:t="top-center",toastOptions:r,gutter:a,children:o,containerStyle:n,containerClassName:s})=>{let{toasts:i,handlers:l}=ea(r);return C.createElement("div",{style:{position:"fixed",zIndex:9999,top:16,left:16,right:16,bottom:16,pointerEvents:"none",...n},className:s,onMouseEnter:l.startPause,onMouseLeave:l.endPause},i.map(r=>{let n=r.position||t,s=ey(n,l.calculateOffset(r,{reverseOrder:e,gutter:a,defaultPosition:t}));return C.createElement(eg,{id:r.id,key:r.id,onHeightUpdate:l.updateHeight,className:r.visible?ev:"",style:s},"custom"===r.type?q(r.message,r):o?o(r):C.createElement(ex,{toast:r,position:n}))}))},ew=r(1944),eC=r(3047);function ej(e){let{views:t}=e,r=(0,w.useRouter)(),{id:a}=r.query,[o,n]=(0,C.useState)(!1);return(0,i.jsxs)("div",{className:"z-10 fixed bottom-5 inset-x-0 mx-auto max-w-fit rounded-lg px-3 py-2 bg-white border border-gray-100 shadow-md flex justify-between space-x-2 items-center",children:[(0,i.jsxs)(b(),{href:"/",className:"flex space-x-3 items-center justify-center font-medium text-gray-600 px-3 h-14 rounded-md hover:bg-gray-100 active:bg-gray-200 transition-all",children:[(0,i.jsx)(c(),{alt:"ShareGPT logo",src:"/logo.svg",width:20,height:20,className:"rounded-sm"}),(0,i.jsx)("p",{children:"Shared via ShareGPT"})]}),(0,i.jsx)("div",{className:"border-l border-gray-200 h-10 w-1"}),(0,i.jsxs)("button",{onClick:()=>navigator.clipboard.writeText("https://shareg.pt/".concat(a)).then(()=>{ee.success("Link copied to clipboard"),n(!0),setTimeout(()=>n(!1),2e3)}),className:"p-2 flex flex-col space-y-1 items-center rounded-md w-12 hover:bg-gray-100 active:bg-gray-200 transition-all",children:[o?(0,i.jsx)(x,{className:"h-4 w-4 text-green-600"}):(0,i.jsx)(g,{className:"h-4 w-4 text-gray-600"}),(0,i.jsx)("p",{className:"text-center text-gray-600 text-sm",children:"Copy"})]}),(0,i.jsx)(ew.Z,{id:a}),(0,i.jsxs)("div",{className:"cursor-default p-2 flex flex-col space-y-1 items-center rounded-md w-12 hover:bg-gray-100 active:bg-gray-200 transition-all",children:[(0,i.jsx)(y.Z,{className:"h-4 w-4 text-gray-600"}),(0,i.jsx)("p",{className:"text-center text-gray-600 text-sm",children:(0,eC.pF)(t)})]})]})}var eN=r(1611),eE=r(7462),ek=r(6652),eT=r(2707),eP=r(9920),eS=r(9312),eM=r(6233),e_=r(958),eO=r(3216),eL=r(192),eD=r(5019);let eR="HoverCard",[eZ,eI]=(0,eT.b)(eR,[eM.D7]),ez=(0,eM.D7)(),[eF,eq]=eZ(eR),e$=e=>{let{__scopeHoverCard:t,children:r,open:a,defaultOpen:o,onOpenChange:n,openDelay:s=700,closeDelay:i=300}=e,l=ez(t),c=(0,C.useRef)(0),d=(0,C.useRef)(0),u=(0,C.useRef)(!1),m=(0,C.useRef)(!1),[p=!1,f]=(0,eP.T)({prop:a,defaultProp:o,onChange:n}),h=(0,C.useCallback)(()=>{clearTimeout(d.current),c.current=window.setTimeout(()=>f(!0),s)},[s,f]),x=(0,C.useCallback)(()=>{clearTimeout(c.current),u.current||m.current||(d.current=window.setTimeout(()=>f(!1),i))},[i,f]),g=(0,C.useCallback)(()=>f(!1),[f]);return(0,C.useEffect)(()=>()=>{clearTimeout(c.current),clearTimeout(d.current)},[]),(0,C.createElement)(eF,{scope:t,open:p,onOpenChange:f,onOpen:h,onClose:x,onDismiss:g,hasSelectionRef:u,isPointerDownOnContentRef:m},(0,C.createElement)(eM.fC,l,r))},eH=(0,C.forwardRef)((e,t)=>{let{__scopeHoverCard:r,...a}=e,o=eq("HoverCardTrigger",r),n=ez(r);return(0,C.createElement)(eM.ee,(0,eE.Z)({asChild:!0},n),(0,C.createElement)(eL.WV.a,(0,eE.Z)({"data-state":o.open?"open":"closed"},a,{ref:t,onPointerEnter:(0,ek.M)(e.onPointerEnter,eJ(o.onOpen)),onPointerLeave:(0,ek.M)(e.onPointerLeave,eJ(o.onClose)),onFocus:(0,ek.M)(e.onFocus,o.onOpen),onBlur:(0,ek.M)(e.onBlur,o.onClose),onTouchStart:(0,ek.M)(e.onTouchStart,e=>e.preventDefault())})))}),eV="HoverCardPortal",[eA,eG]=eZ(eV,{forceMount:void 0}),eB=e=>{let{__scopeHoverCard:t,forceMount:r,children:a,container:o}=e,n=eq(eV,t);return(0,C.createElement)(eA,{scope:t,forceMount:r},(0,C.createElement)(eO.z,{present:r||n.open},(0,C.createElement)(e_.h,{asChild:!0,container:o},a)))},eW="HoverCardContent",eX=(0,C.forwardRef)((e,t)=>{let r=eG(eW,e.__scopeHoverCard),{forceMount:a=r.forceMount,...o}=e,n=eq(eW,e.__scopeHoverCard);return(0,C.createElement)(eO.z,{present:a||n.open},(0,C.createElement)(eU,(0,eE.Z)({"data-state":n.open?"open":"closed"},o,{onPointerEnter:(0,ek.M)(e.onPointerEnter,eJ(n.onOpen)),onPointerLeave:(0,ek.M)(e.onPointerLeave,eJ(n.onClose)),ref:t})))}),eU=(0,C.forwardRef)((e,t)=>{let{__scopeHoverCard:r,onEscapeKeyDown:o,onPointerDownOutside:n,onFocusOutside:s,onInteractOutside:i,...l}=e,c=eq(eW,r),d=ez(r),u=(0,C.useRef)(null),m=(0,eS.e)(t,u),[p,f]=(0,C.useState)(!1);return(0,C.useEffect)(()=>{if(p){let e=document.body;return a=e.style.userSelect||e.style.webkitUserSelect,e.style.userSelect="none",e.style.webkitUserSelect="none",()=>{e.style.userSelect=a,e.style.webkitUserSelect=a}}},[p]),(0,C.useEffect)(()=>{if(u.current){let e=()=>{f(!1),c.isPointerDownOnContentRef.current=!1,setTimeout(()=>{var e;let t=(null===(e=document.getSelection())||void 0===e?void 0:e.toString())!=="";t&&(c.hasSelectionRef.current=!0)})};return document.addEventListener("pointerup",e),()=>{document.removeEventListener("pointerup",e),c.hasSelectionRef.current=!1,c.isPointerDownOnContentRef.current=!1}}},[c.isPointerDownOnContentRef,c.hasSelectionRef]),(0,C.useEffect)(()=>{if(u.current){let e=function(e){let t=[],r=document.createTreeWalker(e,NodeFilter.SHOW_ELEMENT,{acceptNode:e=>e.tabIndex>=0?NodeFilter.FILTER_ACCEPT:NodeFilter.FILTER_SKIP});for(;r.nextNode();)t.push(r.currentNode);return t}(u.current);e.forEach(e=>e.setAttribute("tabindex","-1"))}}),(0,C.createElement)(eD.XB,{asChild:!0,disableOutsidePointerEvents:!1,onInteractOutside:i,onEscapeKeyDown:o,onPointerDownOutside:n,onFocusOutside:(0,ek.M)(s,e=>{e.preventDefault()}),onDismiss:c.onDismiss},(0,C.createElement)(eM.VY,(0,eE.Z)({},d,l,{onPointerDown:(0,ek.M)(l.onPointerDown,e=>{e.currentTarget.contains(e.target)&&f(!0),c.hasSelectionRef.current=!1,c.isPointerDownOnContentRef.current=!0}),ref:m,style:{...l.style,"--radix-hover-card-content-transform-origin":"var(--radix-popper-transform-origin)",userSelect:p?"text":void 0,WebkitUserSelect:p?"text":void 0}})))}),eK=(0,C.forwardRef)((e,t)=>{let{__scopeHoverCard:r,...a}=e,o=ez(r);return(0,C.createElement)(eM.Eh,(0,eE.Z)({},o,a,{ref:t}))});function eJ(e){return t=>"touch"===t.pointerType?void 0:e()}function eY(e){let{content:t,children:r}=e;return(0,i.jsxs)(e$,{openDelay:100,closeDelay:200,children:[(0,i.jsx)(eH,{asChild:!0,children:r}),(0,i.jsx)(eB,{children:(0,i.jsxs)(eX,{align:"center",sideOffset:4,className:"radix-side-top:animate-slide-down-fade radix-side-bottom:animate-slide-up-fade z-10",children:[(0,i.jsx)(eK,{className:"fill-current text-white dark:text-gray-800"}),t]})})]})}function eQ(e){let{comment:t,hoverCard:r,fullComment:a}=e,o=(0,w.useRouter)();return(0,i.jsxs)("div",{className:"w-full ".concat(r?"sm:w-96 shadow-md":"shadow-sm"," rounded-lg p-6 border border-gray-200 bg-white"),children:[(0,i.jsxs)("div",{className:"flex space-x-3 items-center",children:[(0,i.jsx)(c(),{src:t.user.image,width:24,height:24,draggable:"false",className:"w-9 h-9 object-cover rounded-full overflow-hidden",alt:"Profile picture of ".concat(t.user.username)}),(0,i.jsxs)("div",{children:[(0,i.jsx)("p",{className:"font-semibold leading-5",children:t.user.name}),(0,i.jsxs)("p",{className:"text-gray-500 text-sm",children:["@",t.user.username," • ",(0,i.jsx)("span",{children:(0,eC.Sy)(t.createdAt)})]})]})]}),(0,i.jsx)("div",{className:"mt-4",children:(0,i.jsx)("p",{className:"".concat(a?"":"line-clamp-5"," text-sm text-gray-600 whitespace-pre-line"),children:t.content})}),!a&&(0,i.jsx)("button",{onClick(){o.replace({pathname:"/c/[id]",query:{id:o.query.id,comment:t.id}},void 0,{shallow:!0,scroll:!1})},className:"mt-3 bg-white text-black border border-gray-200 hover:bg-gray-50 text-center shadow-sm w-full text-sm h-8 rounded-md transition-all duration-75 focus:outline-none",children:"View Comment"})]})}var e0=r(8568),e1=r(8038);function e2(e){let{comment:t}=e;return(0,i.jsx)(eY,{content:(0,i.jsx)(eQ,{comment:t,hoverCard:!0}),children:(0,i.jsx)(e0.E.div,{variants:e1.fV,className:"inline-block h-8 w-8 rounded-full overflow-hidden ring-2 ring-white",children:(0,i.jsx)(c(),{src:t.user.image,width:24,height:24,draggable:"false",className:"w-full h-full",alt:"Profile picture of ".concat(t.user.username)})})})}var e3=r(5097);function e4(e){let{position:t,count:r}=e,a=(0,w.useRouter)();return(0,i.jsx)(e0.E.button,{onClick(){a.replace({pathname:"/c/[id]",query:{id:a.query.id,position:t}},void 0,{shallow:!0,scroll:!1})},variants:e1.fV,className:"h-9 w-9 flex items-center justify-center rounded-full overflow-hidden border border-gray-200 shadow-sm bg-white text-[11px] text-gray-500 font-medium",children:r>0?(0,i.jsxs)("p",{children:["+",(0,eC.pF)(Math.min(r,99))]}):(0,i.jsx)(e3.Z,{className:"w-4 h-4"})})}var e5=r(1544);function e9(e){let{position:t,comments:r}=e;return(0,i.jsx)(e5.M,{children:r&&(0,i.jsxs)(e0.E.div,{initial:"hidden",whileInView:"show",viewport:{once:!0},className:"hidden absolute top-10 right-0 lg:flex -space-x-1 items-center justify-start lg:w-40 xl:w-60",variants:{hidden:{},show:{transition:{staggerChildren:.05}}},children:[r.slice(0,3).map(e=>(0,i.jsx)(e2,{comment:e},e.id)),(0,i.jsx)(e4,{position:t,count:r.length-3})]},"position-".concat(r.length))})}var e8=r(4483),e6=r.n(e8),e7=r(6513);function te(e){let{children:t,showModal:r,setShowModal:a}=e,o=(0,w.useRouter)(),{comment:n,position:s}=o.query,l=(0,C.useRef)(null),c=(0,C.useRef)(null),d=(0,C.useCallback)(()=>{n||s?o.replace({pathname:"/c/[id]",query:{id:o.query.id}},void 0,{shallow:!0,scroll:!1}):a(!1)},[n,s,o,a]),u=(0,e7._)(),m={type:"spring",stiffness:500,damping:30};async function p(e,t){let r=t.offset.y,a=t.velocity.y,o=l.current.getBoundingClientRect().height;r>o/2||a>800?(await u.start({y:"100%",transition:m}),d()):u.start({y:0,transition:m})}return(0,C.useEffect)(()=>{u.start({y:0,transition:m})},[]),(0,i.jsx)(e5.M,{mode:"wait",children:r&&(0,i.jsx)(e6(),{focusTrapOptions:{initialFocus:!1,clickOutsideDeactivates:!0},children:(0,i.jsxs)("div",{className:"absolute",children:[(0,i.jsxs)(e0.E.div,{ref:l,className:"group fixed inset-x-0 bottom-0 z-40 w-screen cursor-grab active:cursor-grabbing sm:hidden",initial:{y:"100%"},animate:u,exit:{y:"100%"},transition:m,drag:"y",dragDirectionLock:!0,onDragEnd:p,dragElastic:{top:0,bottom:1},dragConstraints:{top:0,bottom:0},children:[(0,i.jsxs)("div",{className:"h-7 bg-white rounded-t-4xl -mb-1 flex w-full items-center justify-center border-t border-gray-200",children:[(0,i.jsx)("div",{className:"-mr-1 h-1 w-6 rounded-full bg-gray-300 transition-all group-active:rotate-12"}),(0,i.jsx)("div",{className:"h-1 w-6 rounded-full bg-gray-300 transition-all group-active:-rotate-12"})]}),t]},"mobile-modal"),(0,i.jsx)(e0.E.div,{ref:c,className:"fixed top-0 right-0 z-40 hidden sm:block h-screen p-5",initial:{translateX:2e3},animate:{translateX:0},exit:{translateX:2e3},children:t},"desktop-modal")]})})})}var tt=r(8814);let tr=(0,h.Z)("X",[["line",{x1:"18",y1:"6",x2:"6",y2:"18",key:"1o5bob"}],["line",{x1:"6",y1:"6",x2:"18",y2:"18",key:"z4dcbv"}]]),ta=(0,h.Z)("Send",[["line",{x1:"22",y1:"2",x2:"11",y2:"13",key:"10auo0"}],["polygon",{points:"22 2 15 22 11 13 2 9 22 2",key:"12uapv"}]]);var to=r(2421),tn=r(9734);let ts=e=>{let{showCommentModal:t,setShowCommentModal:r}=e,a=(0,w.useRouter)(),{id:o,comment:n,position:s}=a.query,{data:l}=(0,tn.ZP)("/api/conversations/".concat(o,"/comments"),eC._i),d=(0,C.useRef)(null),[u,m]=(0,C.useState)(!1),[p,f]=(0,C.useState)(""),h=async e=>{e.preventDefault(),m(!0),await fetch("/api/conversations/".concat(o,"/comments"),{method:"POST",headers:{"Content-Type":"application/json"},body:JSON.stringify({position:s?parseInt(s):1,content:p})}).then(e=>e.json()).then(e=>{m(!1),e.error?ee.error(e.error):(0,tn.JG)("/api/conversations/".concat(o,"/comments")).then(()=>{var e;f(""),null===(e=d.current)||void 0===e||e.scrollIntoView({behavior:"smooth"})})})},x=async e=>{if("Enter"===e.key&&e.metaKey)return await h(e)};return(0,i.jsx)(te,{showModal:t,setShowModal:r,children:(0,i.jsxs)("div",{className:"relative flex flex-col w-full sm:w-[28rem] h-full bg-gray-50 rounded-lg border border-gray-300 shadow-md",children:[(0,i.jsxs)("div",{className:"flex justify-between items-center px-4 h-16 bg-white rounded-t-lg border-b border-gray-200",children:[l&&n?(0,i.jsxs)("button",{onClick(){var e;return a.replace({pathname:"/c/[id]",query:{id:a.query.id,position:(null===(e=l.find(e=>e.id===n))||void 0===e?void 0:e.position)||1}},void 0,{shallow:!0,scroll:!1})},className:"flex space-x-2 items-center rounded-lg hover:bg-gray-100 transition-all p-2",children:[(0,i.jsx)(tt.Z,{className:"w-5 h-5 text-gray-500"}),(0,i.jsx)("h2",{className:"font-medium text-gray-500",children:"Back to all comments"})]}):(0,i.jsx)("h2",{className:"text-xl font-medium text-gray-600 p-2",children:"Comments"}),(0,i.jsx)("button",{onClick:()=>a.replace({pathname:"/c/[id]",query:{id:a.query.id}},void 0,{shallow:!0,scroll:!1}),className:"rounded-lg hover:bg-gray-100 transition-all p-2",children:(0,i.jsx)(tr,{className:"w-5 h-5 text-gray-600"})})]}),l&&s&&(0,i.jsxs)("div",{className:"flex flex-1 flex-col space-y-5 px-3 py-6 overflow-scroll",children:[l.filter(e=>e.position===parseInt(s)).length>0?l.filter(e=>e.position===parseInt(s)).map(e=>(0,i.jsx)(eQ,{comment:e},e.id)):(0,i.jsxs)("div",{className:"flex flex-1 flex-col items-center justify-center space-y-2",children:[(0,i.jsx)(c(),{src:"/illustrations/empty.svg",width:200,height:200,alt:"No comments yet"}),(0,i.jsx)("h2",{className:"text-xl font-medium text-gray-600",children:"No comments yet"}),(0,i.jsx)("p",{className:"text-sm text-gray-500",children:"Be the first to comment on this conversation"})]}),(0,i.jsx)("div",{ref:d})]}),l&&n&&(0,i.jsx)("div",{className:"flex flex-1 flex-col space-y-5 px-3 py-6 overflow-scroll",children:n&&(0,i.jsx)(eQ,{comment:l.find(e=>e.id===n),fullComment:!0},n)}),!n&&(0,i.jsxs)("form",{onSubmit:h,className:"h-60 w-full border-t border-gray-200 p-4 bg-white rounded-b-lg",children:[(0,i.jsx)("textarea",{name:"comment",className:"w-full h-full resize-none text-sm text-gray-600 placeholder:text-gray-400 focus:outline-none border-none focus:ring-0",placeholder:s?"Write a comment...":"Write a reply...",rows:6,required:!0,onKeyDown:x,value:p,onChange(e){f(e.target.value)}}),(0,i.jsx)("button",{className:"absolute bottom-4 right-4 w-10 h-10 flex items-center justify-center border border-gray-300 bg-white rounded-full hover:scale-105 active:scale-100 transition-all",children:u?(0,i.jsx)(to.xg,{}):(0,i.jsx)(ta,{className:"w-4 h-4 mt-0.5 mr-0.5 text-gray-400"})})]})]})})};var ti=!0;function tl(e){let{id:t,content:{avatarUrl:r,items:a},comments:o,views:n}=e;!function(){let e=(0,w.useRouter)(),{id:t}=e.query;(0,C.useEffect)(()=>{t&&fetch("/api/conversations/".concat(t,"/view"),{method:"POST",headers:{"Content-Type":"application/json"}})},[t])}();let{data:s}=(0,tn.ZP)("/api/conversations/".concat(t,"/comments"),eC._i,{fallbackData:o}),l=(0,w.useRouter)(),{comment:d,position:p}=l.query,{CommentModal:h,setShowCommentModal:x}=function(){let[e,t]=(0,C.useState)(!1),r=(0,C.useCallback)(()=>(0,i.jsx)(ts,{showCommentModal:e,setShowCommentModal:t}),[e,t]);return(0,C.useMemo)(()=>({setShowCommentModal:t,CommentModal:r}),[t,r])}();(0,C.useEffect)(()=>{d||p?x(!0):x(!1)},[d,p,x]);let g=(0,C.useMemo)(()=>{var e;return p?parseInt(p):d?(null===(e=null==s?void 0:s.find(e=>e.id===d))||void 0===e?void 0:e.position)||1:null},[d,s,p]);return(0,i.jsxs)(i.Fragment,{children:[(0,i.jsx)(eN.Z,{title:"Check out this ShareGPT conversation",description:"This is a conversation between a human and a GPT-3 chatbot. The human first asks: ".concat(a[0].value,". The GPT-3 chatbot then responds: ").concat(a[1].value),image:"https://sharegpt.com/api/conversations/".concat(t,"/og"),imageAlt:"This is a preview image for a conversation betwen a human and a GPT-3 chatbot. The human first asks: ".concat(a[0].value,". The GPT-3 chatbot then responds: ").concat(a[1].value),canonical:"https://sharegpt.com/c/".concat(t)}),(0,i.jsx)(h,{}),(0,i.jsx)(eb,{}),(0,i.jsx)("div",{className:"flex flex-col items-center pb-24 dark:bg-[#343541] min-h-screen",children:a.map((e,t)=>(0,i.jsxs)("div",{id:t.toString(),className:u()("relative dark:bg-[#343541] text-gray-700 w-full border-b dark:border-gray-700 border-gray-200",{"bg-gray-100 dark:bg-[#434654]":"gpt"===e.from}),children:[(0,i.jsx)(e5.M,{children:g&&g!==t+1&&(0,i.jsx)(e0.E.div,{...e1.kF,className:"absolute w-full h-full z-10 bg-gray-300 dark:bg-black/30 bg-opacity-50 backdrop-blur-[2px] pointer-events-none"})}),(0,i.jsxs)("div",{className:"relative mx-auto max-w-screen-xl dark:text-gray-100 text-gray-700 w-full px-4 py-10",children:[(0,i.jsxs)("div",{className:"w-full max-w-screen-md flex flex-1 mx-auto gap-[1.5rem] leading-[1.75]",children:["human"===e.from?(0,i.jsx)(c(),{className:"mr-2 rounded-sm h-[28px]",alt:"Avatar of the person chatting",width:"28",height:"28",src:r}):(0,i.jsx)(m,{}),(0,i.jsx)("div",{className:"flex flex-col",children:"human"===e.from?(0,i.jsx)("p",{className:"pb-2",children:e.value}):(0,i.jsx)("div",{className:f().response,dangerouslySetInnerHTML:{__html:e.value}})})]}),(0,i.jsx)(e9,{position:t+1,comments:null==s?void 0:s.filter(e=>e.position===t+1)})]})]},e.value))}),(0,i.jsx)(ej,{views:n})]})}},2734:function(e){e.exports={response:"utils_response__b5jEi"}}},function(e){e.O(0,[696,377,774,888,179],function(){return e(e.s=3164)}),_N_E=e.O()}]);